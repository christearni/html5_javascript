(function () {
    'use strict';
    const Observable = Rx.Observable;

    function Component1(dataService) {

        var context = this;

        this.dataService = dataService;

        var sendButton = document.getElementById('sendButton');

        this.pings$ = Observable.interval(3000)
            .do(function(){console.log("Component1: sending ping");})
            .map(function(){return new Date();});

        this.messages$ = Observable.fromEvent(sendButton, 'click')
            .do(function() {console.log("Component1: sending message");})
            .do(function () { context.dataService.getData().subscribe(render);})
            .map(function(){return new Date()});

        function render(data) {
            var dataDiv = document.createElement('div');
            dataDiv.textContent = data;
            document.getElementById('data').appendChild(dataDiv);
        }
    }

    function Component2(component1) {

        Observable.merge(component1.pings$, component1.messages$)
            .scan(function (acc, cur) {
                render('Message ' + acc + ': ' + cur);
                return acc + 1;
            }, 1)
            .subscribe();

        function render(message){
            document.getElementById('message').textContent = message;
        }
    }

    function DataService() {
        this.getData = function() {
            console.log('DataSevice: getting data ...');
            return Observable.fromPromise(
                fetch('https://api.github.com/repos/angular/angular.js/commits')
                    .then(function(r){ return r.json();})
            );
        }
    }


    const dataService = new DataService();
    const component1 = new Component1(dataService);
    const component2 = new Component2(component1);

})();
