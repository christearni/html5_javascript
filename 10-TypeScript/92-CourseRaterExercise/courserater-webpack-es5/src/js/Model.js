export var model = {
    ratings: [],
    addRating: addRating,
    removeRating: removeRating,
};
createNewRating();

function addRating() {
    model.ratings.push(model.newRating);
    createNewRating();
    notifyChange();
}

function removeRating(ratingId) {
    var index = model.ratings.findIndex(function (r) {
        return r.id === ratingId;
    });
    model.ratings.splice(index, 1);
    notifyChange();
}

function createNewRating() {
    model.newRating = {name: '', grade: undefined};
    model.newRating.id = getId();
}

function getId() {
    return Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
}

function notifyChange(){
    $(model).trigger('change');
}
