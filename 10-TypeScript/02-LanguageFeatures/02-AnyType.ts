{
    let v: any = 42;
    v = 'Hello!'; // anything can be assigned to v


    let n: number = 43;
    n = v; // v can be assigned to anything!!!


    let list: any[] = [1, true, "free"];


    let x; // implicit any type
    let y = 42; // type is deferred

    x = 42;
    x = 'Hello';

    y = 43;
    // y = 'Hello';
}