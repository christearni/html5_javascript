// See: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

const getBtn = document.getElementById('get');
const postBtn = document.getElementById('post');
const idInput = document.getElementById('id');
const putBtn = document.getElementById('put');
const deleteBtn = document.getElementById('delete');

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', postData);
putBtn.addEventListener('click', putData);
deleteBtn.addEventListener('click', deleteData);


function getData() {

    $.get('http://localhost:3001/comments')
        .then(function (data) {
            console.log(data);
        });
}

function postData() {

    $.post('http://localhost:3001/comments', {text: 'test - ' + new Date()})
        .then(function () {
            console.log('POSTED!');
        });
}

function putData() {
    var id = idInput.value;

    $.ajax({
        url: 'http://localhost:3001/comments/' + id,
        type: 'PUT',
        data: {text: 'test - ' + new Date()}
    })
    .then(function () {
        console.log('PUT!')
    });
}

function deleteData() {
    var id = idInput.value;

    $.ajax({
        url: 'http://localhost:3001/comments/' + id,
        type: 'DELETE'
    })
    .then(function (result) {
        console.log('DELETED!')
    });
}
