lesson('about string templates in ES6', () => {

    learn('strings support templating', function () {

        let name = 'Bob';
        let age = 18;

        let message = `In ${(new Date).getFullYear() + 1} ${name} will be ${age + 1} `;

        expect(message).toBe(FILL_ME_IN);
    });

    learn('template strings can have multiple lines', function () {

        let name = 'Bob';
        let age = 18;

        let message = `In ${(new Date).getFullYear() + 1}
        ${name} is currenty ${age + 1}
        This is amazing!
        `;

        expect(message).toBe(FILL_ME_IN);
    });

});

