const webpack = require('webpack');
const path = require('path');

module.exports = {
	entry: './app.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					babelrc: false,
					presets: [['es2015', {"modules":false}]]
				}
			}
		]
	}
};
