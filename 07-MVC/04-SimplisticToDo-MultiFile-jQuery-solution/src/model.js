(function (global) {
    'use strict';

    function createModel() {

        var newTodo = {text: ''};
        var todoList = [];

        var model = {
            getNewTodo: function () {
                return newTodo;
            },
            getTodoList: function () {
                return todoList
            },
            updateNewTodo: updateNewTodo,
            addNewTodo: addNewTodo,
            removeTodoAtIndex: removeTodoAtIndex,
            todos: [],
            newTodo: {text: ''}
        };
        var $model = $(model);
        return model;

        function updateNewTodo(text) {
            newTodo.text = text;
            $model.trigger('changed');
        }

        function addNewTodo() {
            todoList.push(newTodo);
            newTodo = {text: ''};
            $model.trigger('changed');
        }

        function removeTodoAtIndex(index) {
            todoList.splice(index, 1);
            $model.trigger('changed');
        }
    }

    global.todo = global.todo || {};
    global.todo.createModel = createModel;

})(window);
