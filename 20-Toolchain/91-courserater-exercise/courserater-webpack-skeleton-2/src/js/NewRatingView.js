import $ from 'jquery';

export class NewRatingView {

    constructor() {
        this.nameInput = $('#name');
        this.gradeInput = $('#grade');
        this.addBtn = $('#addBtn');

        const changeRating = () => $(this).trigger('ratingChanged', {name: this.nameInput.val(), grade: parseInt(this.gradeInput.val())});
        const addRating = () => $(this).trigger('ratingAdded');
        this.nameInput.on('blur', changeRating);
        this.gradeInput.on('blur', changeRating);
        this.addBtn.on('click', addRating);
    }

    render(rating) {
        this.nameInput.val(rating.name);
        this.gradeInput.val(rating.grade);
    }
}
