const input = document.getElementById('input');
const resultList = document.getElementById('result');

var observable = Rx.Observable.fromEvent(input, 'keyup');

observable
    .do(event => console.log('Key: ' + event))
    .map(event => event.target.value)
    .do(val => console.log('Value: ' + val))
    .debounceTime(400)
    .do(val => console.log('Debounced: ' + val))
    .distinctUntilChanged()
    .switchMap(term => Rx.Observable.fromPromise(searchWikipedia(term)))
    .do(data => console.log('Data: ' + JSON.stringify(data).substring(1,100)))
    .subscribe(data => displayResults(data[1]));


function displayResults(results) {

    result.innerHTML = '';
    if(!results) return;

    for (let result of results) {
        const li = document.createElement('li');
        li.innerText = result;
        resultList.appendChild(li);
    }
}

function searchWikipedia(term){
    return new Promise((resolve, reject) => {
        JSONP({
            url: 'https://en.wikipedia.org/w/api.php',
            data: {
                search: term,
                action: 'opensearch',
                format: 'json'
            },
            success: function (data) {

                if (term.length > 4){
                    setTimeout(() => resolve(data), 2000)
                }
                else {
                    resolve(data);
                }
            }
        });
    })
}


/* solution */
// start with map -> the stream contains promises and not the resolved result -> flatMap
// introduce debounceTime
// introduce distinctUntilChanged -> does not work, because the event is changed every time -> map to value at the beginning
// show out of order problem with search term length > 4 -> switchMap
