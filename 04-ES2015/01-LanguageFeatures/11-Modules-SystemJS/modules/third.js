console.log('loading third module');

export default () => console.log('Hello from third module!');


// There can only be one default export
// export default {
//     sayHello: () => console.log('Hello from third module!'),
//     sayBye: () => console.log('Bye from third module!')
// };


// There can be other export besides a default export
export let greeter = {
    sayHello: () => console.log('Hello from third module greeter!'),
    sayBye: () => console.log('Bye from third module greeter!')
};


// The module could be refactored to extract the functions