'use strict';


import {showApp} from './js/base';
import {bootstrap} from './js/app';

bootstrap();
showApp();