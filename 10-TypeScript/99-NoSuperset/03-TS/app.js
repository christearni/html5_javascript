'use strict';
var Person = (function () {
    function Person(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    Person.prototype.fullName = function () {
        return this.firstName + " " + this.lastName;
    };
    Person.prototype.fullNameReversed = function () {
        return this.lastName + " " + this.firstName;
    };
    return Person;
})();
var person = new Person('Tyler', 'Durden');
var button = document.createElement('button');
button.textContent = "Click Me!";
button.onclick = function () { return alert(person.fullName()); };
document.body.appendChild(button);
//# sourceMappingURL=app.js.map