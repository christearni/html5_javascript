// ES5
var out = [];

for (var i = 0; i < 10; i++){
    out.push(function(){
        // a variable declared with var is function/global scoped
        console.log(i);
    })
}

out.forEach(function(f){
    f();
});


// ES6
var out = [];

for (let i = 0; i < 10; i++){
    out.push(function(){
        // a variable declared with let is block-scoped
        // an automatic closure is created
        console.log(i);
    })
}

out.forEach(function(f){
    f();
});
