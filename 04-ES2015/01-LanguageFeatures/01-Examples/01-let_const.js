// ES5
var x = 'hi!';

if(true){
    var x = 'bye!';
}

console.log(x);


// ES6
let y = 'hi!';
const z = 'hi!'

if(true){
    let y = 'bye!';
    const z = 'bye!'
    //z = 'no!' // not allowed
}

//let x = 'no!'; // not allowed

console.log(y);
console.log(z);
