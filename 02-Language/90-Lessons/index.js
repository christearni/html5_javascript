var jsLessons;
(function (jsLessons) {

    jsLessons.lessonFiles = [
        '01-Basics.js',
        '02-Arrays.js',
        '03-Objects.js',
        '04-Functions.js',
        '05-HigherOrderFunctions.js',
        '06-ClassesAndNamespaces.js',
        '07-Inheritance.js',
        '08-Patterns.js',
        '09-BadParts.js'
    ];

    jsLessons.start();


})(jsLessons || (jsLessons = {}));