lesson("about classes in ES6", () => {

    learn("how a class is used in ES6", () => {

        class Person {
            name;
            constructor(name) {
                this.name = name;
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }
        }

        let p = new Person('Bob');

        expect(p.greet()).toBe(FILL_ME_IN);
    });

    learn("that classes can have static methods", () => {

        class Person {
            name;
            constructor(name) {
                this.name = name;
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }

            static smallTalk(peer) {
                return `Hey ${peer}, the weather is nice today! `;
            }
        }

        expect(Person.smallTalk('Alice')).toBe(FILL_ME_IN);

        const p1 = new Person('Bob');
        expect(typeof Person.smallTalk).toBe(FILL_ME_IN);
        expect(typeof p1['greet']).toBe(FILL_ME_IN); // static methods are no available on an instance
        expect(typeof p1['smallTalk']).toBe(FILL_ME_IN); // static methods are no available on an instance

    });

    learn("that classes can have setters/getters", () => {

        class Person {
            first;
            last;
            constructor(first, last) {
                this.first = first;
                this.last = last;
            }

            get full() {
                return this.first + ' ' + this.last;
            }

            set full(val) {
                let parts = val.split(' ');
                this.first = parts[0];
                this.last = parts[1];
            }
        }

        let p = new Person('Tyler', 'Durden');
        expect(p.full).toBe(FILL_ME_IN);

        p.full = 'Bob Bobbing';
        expect(p.first).toBe(FILL_ME_IN);
        expect(p.last).toBe(FILL_ME_IN);

    });

    learn("how to use inheritance with classes", () => {

        class Polygon {
            name;
            height;
            width;
            constructor(height, width) {
                this.name = 'Polygon';
                this.height = height;
                this.width = width;
            }

            describe() {
                return `I am a ${this.name}.`;
            }

            static convert(value) {
                return value * 1.3;
            }
        }

        class Square extends Polygon {
            constructor(length) {
                super(length, length); //call the parent method with super
                this.name = 'Square';
            }

            get area() {
                return this.height * this.width;
            }
        }

        let s = new Square(5);
        expect(s.describe()).toBe(FILL_ME_IN);

    });


    learn("how `this` still is dynamically bound in ES6 classes", () => {
        class Person {
            name;
            constructor(name) {
                this.name = name;
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }
        }

        let p = new Person('Bob');

        expect(p.greet.call({name: 'Alice'})).toBe(FILL_ME_IN);
        // This illustrates the problem from more real-world scenarios like passing greet as an event-handler or callback

    });

    learn("how `this` can be bound to an instance", () => {
        class Person {
            name;
            constructor(name) {
                this.name = name;
                this.greet = this.greet.bind(this);
            }

            greet() {
                return `Hello, my name is ${this.name}`;
            }
        }

        let p = new Person('Bob');

        expect(p.greet.call({name: 'Alice'})).toBe(FILL_ME_IN);
    });

    learn("that class declarations are not hoisted", () => {

        // In ES5 constructor functions are hoisted:
        var p1 = new Person('Katniss'); // use the contructor before it's declaration
        expect(p1.name).toBe(FILL_ME_IN);
        function Person(name) {
            this.name = name
        }


        // In ES6 classes are not hoisted
        expect(typeof Animal).toBe(FILL_ME_IN); // Animal is not yet defined.
        class Animal {
            name;
            constructor(name) {
                this.name = name;
            }
        }
        expect(typeof Animal).toBe(FILL_ME_IN);

    });

    learn("about class declaration and class expressions", () => {

        // typically classes are declared:
        class Animal {
            name;
            constructor(name) {
                this.name = name;
            }
        }
        const a1 = new Animal('Lion');
        expect(a1.name).toBe(FILL_ME_IN);

        // but classes can also defined with a class expression
        const Person = class {
            public name; // Typescript needs the name to be declared as public???
            constructor(name) {
                this.name = name;
            }
        };
        const p1 = new Person('Katniss');
        expect(p1.name).toBe(FILL_ME_IN);
    });

    learn("how to declare static class properties in ES6", () => {

        class Person {
            name;
            constructor(name) {
                this.name = name;
            }

            greet() {
                return `Hello, my name is ${this.name}. ${Person.staticMessage}`;
            }

            // ES6 classes can have static methods
            static smallTalk(peer) {
                return `Hey ${peer}, ${this.staticMessage}`;
            }

            static staticMessage = 'I am static!'; // this does not work in ES2015!!!
        }

        // In ES2015, if you want to define static class properties you have to add them to the class:
        // Person.staticMessage = 'I am static!';

        const p1 = new Person('Bob');
        expect(p1.greet()).toBe(FILL_ME_IN);
        expect(Person.staticMessage).toBe(FILL_ME_IN);
        expect(Person.smallTalk('Alice')).toBe(FILL_ME_IN);

    });

    /// NOTE:
    /// The code below only works if you transpile with Babel and the appropriate babel-plugin
    /// In the test-setup this is the case if you select the 'transpile' checkbox

    // learn("how to use class instance fields and class static properties in ES.next", () => {
    //
    //     // Note that "class instance fields" and "class static properties" is an
    //     // ECMAScript language proposal and not yet standardized:
    //     // https://github.com/jeffmo/es-class-fields-and-static-properties
    //     // Babel however allows you to use that feature already
    //
    //     class Person {
    //
    //         name = 'Bob';
    //         static staticMessage = 'I am static!';
    //
    //         greet() {
    //             return `Hello, my name is ${this.name}. ${Person.staticMessage}`;
    //         }
    //
    //         // ES6 classes can have static methods
    //         static smallTalk(peer) {
    //             return `Hey ${peer}, ${this.staticMessage}`;
    //         }
    //     }
    //
    //     const p1 = new Person();
    //     // expect(p1.greet()).toBe(FILL_ME_IN);
    //     // expect(Person.staticMessage).toBe(FILL_ME_IN);
    //     expect(Person.smallTalk('Alice')).toBe(FILL_ME_IN);
    //
    // });

});

