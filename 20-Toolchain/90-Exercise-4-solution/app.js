import 'bootstrap/dist/css/bootstrap.css';
import $ from 'jquery';

class Greeter {
    getMessage() {
        return 'Hello from jQuery';
    }
}
const greeter = new Greeter();

$('body').append(`<h1 class="alert alert-info"> ${greeter.getMessage()} </h1>`);