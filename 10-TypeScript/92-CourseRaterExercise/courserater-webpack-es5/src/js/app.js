import {ratingController} from "./RatingController";
import {statisticsController} from "./StatisticsController";

ratingController.init();
statisticsController.init();


// Layout Logic
enableTab(location.hash);

function enableTab(hash) {
    if (hash === '') hash = '#main';
    $('.container section').hide();
    $(hash).show();
}

window.addEventListener('hashchange', e => {
    enableTab(location.hash)
});
