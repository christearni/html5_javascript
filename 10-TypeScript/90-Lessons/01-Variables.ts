lesson('about declaring variables with `let` and `const`', () => {

    learn('that variables in ES5 can be declared multiple times', () => {

        var x = 'hi!';
        var x = 'bye!';

        expect(x).toBe(FILL_ME_IN);
    });

    learn('that variables can only declared once with `let`', () => {

        // Fix the code block below, so that no SyntaxError
        // is thrown when it is evaluated:
        let codeBlock = `
                let y = 'hi!';
                let y = 'bye!';
            `;
        eval(codeBlock);
    });


    learn('that variables declared with `const` can not be changed', () => {

        // Fix the code block below, so that no SyntaxError
        // is thrown when it is evaluated:
        let codeBlock = `
                const y = 'hi!';
                y = 'bye!';
            `;
        eval(codeBlock);

    });
});

lesson('about the scope of variables', () => {

    learn('that variables are function scoped in ES5', () => {

        var x = 'hi!';

        if (true) {
            var x = 'bye!';
        }

        expect(x).toBe(FILL_ME_IN);
    });

    learn('that variables declared with `let` are block scoped', () => {

        let x = 'hi!';

        if (true) {
            let x = 'bye!';
        }

        expect(x).toBe(FILL_ME_IN);
    });

    learn('the conequences that variables are function scoped in ES5', () => {

        var out = [];

        for (var i = 0; i < 3; i++) {
            out.push(() => 2 * i);
        }

        expect(out.map(f => f()).join(' ')).toBe(FILL_ME_IN);
    });

    learn('the conequences that variables declared with `let` are block scoped', function () {
        'use strict';
        let out = [];

        for (let i = 0; i < 3; i++) {
            out.push(() => 2 * i);
        }

        expect(out.map(f => f()).join(' ')).toBe(FILL_ME_IN);
    });

});

lesson('about improved array iteration in ES6', () => {

    learn('that `for-in` is unintuitive in ES5', () => {

        var arr = ['a', 'b', 'c'];
        var out = [];

        for (var x in arr){
            out.push(x);
        }

        expect(out.join(' ')).toBe(FILL_ME_IN);
    });

    learn('that `for-of` is intuitive in ES6', () => {

        let arr = ['a', 'b', 'c'];
        let out = [];

        for (let x of arr){
            out.push(x);
        }

        expect(out.join(' ')).toBe(FILL_ME_IN);
    });
});
