(function () {
    'use strict';

    function createModel() {

        var newTodo = { text: '' };
        var todoList = [];

        var model = {
            getNewTodo: function(){return newTodo;},
            getTodoList: function(){return todoList},
            updateNewTodo: updateNewTodo,
            addNewTodo: addNewTodo,
            removeTodoAtIndex: removeTodoAtIndex,
            todos: [],
            newTodo: { text: '' }
        };
        var $model = $(model);

        return model;

        function updateNewTodo(text){
            newTodo.text = text;
            $model.trigger('changed');
        }

        function addNewTodo(){
            todoList.push(newTodo);
            newTodo = { text: '' };
            $model.trigger('changed');
        }

        function removeTodoAtIndex(index){
            todoList.splice(index, 1);
            $model.trigger('changed');
        }
    }

    function TodoView() {

        var self = this;
        var $self = $(self);

        var todoInputElem = $('#input');
        var addBtn = $('#addBtn');

        todoInputElem.on('blur', function () {
            $self.trigger('itemChanged', todoInputElem.val());
        });

        addBtn.on('click', function () {
            $self.trigger('itemAdded');
        });

        self.render = function (todo) {
            todoInputElem.val(todo.text);
        }
    }

    function TodoListView() {

        var self = this;
        var $self = $(self);

        var todoListElem = $('#do');

        self.render = function (todos) {
            var todo;

            todoListElem.html('');
            for (var i = 0, len = todos.length; i < len; i++) {
                todo = todos[i];

                var li = $('<li>');
                li.addClass('clearfix');
                li.text(todo.text);

                var span = $('<span>');
                span.addClass('pull-right');

                var button = $('<button>');
                button.addClass("btn btn-xs btn-danger remove glyphicon glyphicon-trash");
                button.data('index', i);
                button.on('click', (function (index) {
                    // return function () { $self.trigger('itemRemoved', index) };
                    return function () { $self.trigger('itemRemoved', $(this).data('index')) };
                })(i));

                span.append(button);
                li.append(span);
                todoListElem.append(li);
            }
        }
    }

    function SummaryView() {

        var self = this;

        var summaryElem = $('#summary');

        self.render = function (todos) {
            summaryElem.text(todos.length + ' items');
        }
    }


    var controller = {

        init: function () {

            var model = createModel();

            $(model).bind('changed', updateUi);

            var summaryView = new SummaryView();
            var todoView = new TodoView();
            var todoListView = new TodoListView();

            $(todoView).bind('itemChanged', function(event, text){updateTodo(text)});
            $(todoView).bind('itemAdded', addItem);
            $(todoListView).bind('itemRemoved', function(event, index){removeItemAtIndex(index)});

            summaryView.render(model.todos);
            todoView.render(model.newTodo);
            todoListView.render(model.todos);

            function updateTodo(todoText) {
                model.updateNewTodo(todoText);
            }
            function addItem() {
                model.addNewTodo();
            }
            function removeItemAtIndex(index) {
                model.removeTodoAtIndex(index);
            }

            function updateUi(){
                summaryView.render(model.getTodoList());
                todoView.render(model.getNewTodo());
                todoListView.render(model.getTodoList());
            }

        }
    };
    controller.init();
})();
