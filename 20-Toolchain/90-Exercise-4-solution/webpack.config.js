const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: ['jquery', './app.js', './module.js'],
    output: {
        filename: 'dist/bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map",
    // plugins: [
    //     new webpack.ProvidePlugin({
    //         $: 'jquery'
    //     })
    // ]
    module: {
        rules: [
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /(node_modules)/
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            {   test: /\.(woff(2)?|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: "url-loader"
            }
        ]

    }
};