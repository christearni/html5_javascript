import {IRating} from "./Model";

export class NewRatingView {
    private nameInput: JQuery;
    private gradeInput: JQuery;
    private addBtn: JQuery;

    constructor() {
        this.nameInput = $('#name');
        this.gradeInput = $('#grade');
        this.addBtn = $('#addBtn');

        const changeRating = () => $(this).trigger('ratingChanged', {name: this.nameInput.val(), grade: parseInt(this.gradeInput.val())});
        const addRating = () => $(this).trigger('ratingAdded');
        this.nameInput.on('blur', changeRating);
        this.gradeInput.on('blur', changeRating);
        this.addBtn.on('click', addRating);
    }

    public render(rating: IRating) : void {
        this.nameInput.val(rating.name);
        this.gradeInput.val(rating.grade);
    }
}
