//* Modules are loaded according to the dependency tree (top-down)
//* Modules are executed according to the dependency tree bottom up
// import './second.js';

console.log('loading first module');

// modules execute in strict mode:
// undeclared = 'Test';


//* Import a specific construct from a module
// * Note: import statements are hoisted
// import {sayHello} from './second.js';
// sayHello();


//* Imports can be aliased
// import {sayHello as greet} from './second.js';
// greet();
// sayHello();


//* Modules can't be loaded dynamically:
// if ((new Date()).getMilliseconds() % 2) {
    // import './third.js';
// }


//* A module can export a default
//* Modules can export different types:
// import thirdExport from './third.js';
// thirdExport();
// thirdExport.sayHello();
// thirdExport.sayBye();


// Defaults can be renamed and other exports can exist besides a default export
// import {default as thirdExport, greeter} from './third.js';
// thirdExport();
// greeter.sayHello();


//* Import everything on an object
// import * as greeter from './second.js';
// greeter.sayHello();
// greeter.sayGoodbye();


//* Imports are read-only (but exposed objects are not immutable)
// import {greetingMessage, greet, greeter} from './fourth.js';
// greetingMessage = 'Hello World!';
// greet = function () { console.log('test'); };
// greeter = { greet(){ console.log('Changed!')}};
// greeter.greet = () => console.log('Changed!'); // objects are passed by reference...
// greeter.greetingMessage = 'Changed!';
// greeter.greet();
