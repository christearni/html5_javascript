## Exercise 1: Observables
Studieren Sie das Beispiel `08-Async/30-Observables/20-ComponentCommunication`.

Erweitern Sie das Beispiel, so dass im "Component 2" mit der Message ein Counter angezeigt wird, welcher pro gesendeter Message (manuell mit dem Button oder mit dem "Ping") inkrementiert wird.

Verwenden Sie `http://reactivex.io/rxjs/` um die entsprechenden Operationen zu finden.


## Exercise 2: Observables Backend Access

Studieren Sie die Lösung für die "Backend Access" Aufgabe mit Observables.


## Exercise 3: TypeScript from Scratch

Install the TypeScript compiler, write a TypeScript class and inspect the transpiled result:

- Start a new npm project: `npm init`
- Install TypeScript `npm i -D typescript`
- Create the TypeScript configuration: `node_modules/.bin/tsc --init` (or use WebStorm *Create New -> tsconfic.json* and adjust it.
- Create a npm task that transpiles TypeScript to JavaScript with `tsc` 
- Write a ES2015 class and annotate it with type information
- Inspect the transpiled output


## Exercise 4: Language Features

Learn TypeScript features with automated lessons:

- In `10-TypeScript/90-Lessons` execute `npm start` ... a browser should open and display some *automated lessons*
- Edit the TypeScript files `00-Basics.ts` ... to `08-Spread.ts` and watch your progress in the browser.



## Exercise 5: TypeScript with 3rd Party Libraries

In the example `10-TypeScript/91-3rdParty`:

- Run the example:

		npm install
		npm start
	
- A browser should open and you see an error at runtime.
- Rename the file `app.js` to `app.ts` ... do you see any changes?
- Install the type definitions for `lodash`:

		npm install @types/lodash
		
- What did change? Youo should now get a compile error ...



## Exercise 6: WebPack - From ES5 to TypeScript 

Inspect the ToDo-App examples at `10-TypeScript/91-CourseRaterExercise`.

Have a look at `courserater-es5`. Get it running by opening the `index.html`. Study the code.

Now go to `courserater-webpack-es5`. Start the app with;

	npm install
	npm start
	
Study the code. What are the differences to the "plain ES5" solution in `courserater-es5`?  
Compare the setup also with the setup from last lesson: `20-Toolchain/91-courserater-exercise/courserater-webpack-skeleton-2`. Where are the differences in the project setup? 

This example is using WebPack as a module bundler together with the TypeScript loader.  

Start converting `courserater-es5` to TypeScript. Start using classes, interfaces and types.

