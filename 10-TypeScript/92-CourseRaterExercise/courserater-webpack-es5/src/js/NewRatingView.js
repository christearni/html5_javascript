export function NewRatingView() {

    var self = this;

    var nameInput = $('#name');
    var gradeInput = $('#grade');
    var addBtn = $('#addBtn');

    self.render =function(rating){
        nameInput.val(rating.name);
        gradeInput.val(rating.grade);
    };

    nameInput.on('blur', function () {
        changeRating()
    });
    gradeInput.on('blur', function () {
        changeRating()
    });
    addBtn.on('click', function () {
        addRating()
    });


    function changeRating(){
        $(self).trigger('ratingChanged', {name: nameInput.val(), grade: parseInt(gradeInput.val())});
    }

    function addRating() {
        $(self).trigger('ratingAdded');
    }
}
