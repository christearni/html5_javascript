The demos can be run via `index.html` in any modern browser.

The demos can be run in node version >= 6.

To run the demos in an older environment, you have to transpile to ES5.

You can transpile with Babel:

    npm install # just once
    npm run transpile-babel

Then use the transpiled scripts in the direcotry `es5-babel`


Or you can transpile with TypeScript:

    npm install # just once
    npm run transpile-ts

Then use the transpiled scripts in the direcotry `es5-ts`