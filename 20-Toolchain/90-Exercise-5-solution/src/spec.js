describe('Bowling Game', () => {
    var game;
    beforeEach(function(){
       game = new BowlingGame();
    });

    it('should calculate the score for a gutter game', () => {
        for (var i = 0; i < 20; i++){
            game.roll(0);
        }
        expect(game.getScore()).toEqual(0);
    });

    it("should calculate the score for all ones", () => {
        rollMany(20, 1);
        expect(game.getScore()).toEqual(20);
    });

    it("should calculate the score for one spare", () => {
        rollSpare();
        game.roll(3);
        rollMany(17, 0);
        expect(game.getScore()).toEqual(16);
    });

    it("should calculate the score for one strike", () => {
        rollStrike();
        game.roll(3);
        game.roll(4);
        rollMany(16, 0);
        expect(game.getScore()).toEqual(24);
    });

    it("should calculate the score for a perfect game", () => {
        rollMany(12, 10);
        expect(game.getScore()).toEqual(300);
    });

    function rollMany (n, pins) {
        for (var i = 0; i < n; i++) {
            game.roll(pins)
        }
    }

    function rollSpare() {
        game.roll(5);
        game.roll(5);
    }

    function rollStrike() {
        game.roll(10);
    }
});
