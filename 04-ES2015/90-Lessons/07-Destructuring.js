lesson('about destructuring in ES6', () => {

    learn('how an array can be destructured', () => {

        const arr = [1,2];

        // The following statement declares two variables
        const [one, two] = arr;

        expect(one).toBe(FILL_ME_IN);
        expect(two).toBe(FILL_ME_IN);
    });

    learn('how an object can be destructured', () => {

        const obj = {three: 3, four: 4 , five: 5, six: 6};

        // The following statement declares three variables
        const {three, four, five} = obj;

        expect(three).toBe(FILL_ME_IN);
        expect(four).toBe(FILL_ME_IN);
        expect(five).toBe(FILL_ME_IN);
    });

    learn('how an object destructuring can be combined with default values', () => {

        const obj = {three: 3, four: 4 , five: 5, six: 6};

        const {zero = 0, three = 1, seven = 7} = obj;

        expect(zero).toBe(FILL_ME_IN);
        expect(three).toBe(FILL_ME_IN);
        expect(seven).toBe(FILL_ME_IN);

    });

    learn('how an object destructuring can be used for passing default function parameters', () => {

        function processValues({zero = 0, three = 1, seven = 7}){
            expect(zero).toBe(FILL_ME_IN);
            expect(three).toBe(FILL_ME_IN);
            expect(seven).toBe(FILL_ME_IN);
        }

        const obj = {three: 3, four: 4 , five: 5, six: 6};
        processValues(obj);
    });


    learn('how to select properties out of objects with destructuring', () => {

        const persons = [
            {
                firstName: 'Katniss',
                lastName: 'Everdeen',
                district: 12
            },
            {
                firstName: 'Johanna',
                lastName: 'Mason',
                district: 7
            },
            {
                firstName: 'Finnick',
                lastName: 'Odair',
                district: 4
            }
        ];

        const districts = persons.map(({district}) => district);

        expect(districts.join(',')).toBe(FILL_ME_IN);

    });

    learn('how to accept object properties as function arguments with destructuring', () => {
        
        function createMessage({name, district}){
            return `Hello ${name} from district ${district}!`;
        }

        const paramObject = { name: 'Katniss', district: 12};
        const message = createMessage(paramObject);

        expect(message).toBe(FILL_ME_IN);
    });

    learn('that destructuring can be nested', () => {
        const input1 = {val1: 1, val2: {a:8, b:9}, val3: 3};
        const input2 = [42, {val1: 1, val2: {a:8, b:9}, val3: 3}];
        const {val2: {b = 42}} = input1;
        const {val3: {b:b2 = 42}} = input1;
        const {val4: {b:b3 = 42} = {}} = input1;
        const [,{val2: {b:b4 = 42}}] = input2;

        expect(b).toBe(FILL_ME_IN);
        expect(b2).toBe(FILL_ME_IN);
        expect(b3).toBe(FILL_ME_IN);
        expect(b4).toBe(FILL_ME_IN);
    });

    /// NOTE:
    /// The code below only works if you transpile with Babel and the appropriate babel-plugin
    /// In the test-setup this is the case if you select the 'transpile' checkbox

    // learn("how to use rest properties for object destructuring in ES.next", () => {
    
    //     // Object Rest/Spread Properties ist currently a stage 3 proposal for ECMAScript
    //     // https://github.com/sebmarkbage/ecmascript-rest-spread
    //     // Babel however allows you to use that feature already
    
    //     let obj = {three: 3, four: 4 , five: 5, six: 6};
    //     let {three, four, ...rest} = obj;
    
    //     expect(typeof rest).toBe(FILL_ME_IN);
    //     expect(rest.five).toBe(FILL_ME_IN);
    //     expect(rest.six).toBe(FILL_ME_IN);
    // });

});

