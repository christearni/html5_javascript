(function () {
    'use strict';

    /* ======= Model ======= */
    function createModel() {

        var currentFramework = null;
        var frameworks = [
            {
                likeCount: 0,
                name: 'Backbone',
                imgSrc: 'img/backbone.jpg',
                url: 'http://backbonejs.org/'
            },
            {
                likeCount: 0,
                name: 'Marionette',
                imgSrc: 'img/marionette.png',
                url: 'http://marionette.com/'
            },
            {
                likeCount: 0,
                name: 'EmberJS',
                imgSrc: 'img/ember.jpg',
                url: 'http://emberjs.com/'
            },
            {
                likeCount: 0,
                name: 'Knockout',
                imgSrc: 'img/knockout.jpg',
                url: 'http://knockoutjs.com/'
            },
            {
                likeCount: 0,
                name: 'AngularJS',
                imgSrc: 'img/angular.jpg',
                url: 'https://angularjs.org/'
            },
            {
                likeCount: 0,
                name: 'React',
                imgSrc: 'img/react.png',
                url: 'http://facebook.github.io/react/'
            }

        ];

        var model = {
            getFrameworkList: function () { return frameworks; },
            getCurrentFramework: function () { return currentFramework; },
            setCurrentFramework: function (framework) { currentFramework = framework; $model.trigger('change') },
            likeCurrentFramework: function () { currentFramework.likeCount++; $model.trigger('change') }
        };
        var $model = $(model);

        return model;
    }


    /* ======= View ======= */

    function FrameworkView() {

        var self = this;
        var $self = $(self);

        var frameworkNameElem = $('#framework-name');
        var logoImageElem = $('#logo-img');
        var likeCountElem = $('#like-count');
        var likeButtonElem = $('#like-button');

        likeButtonElem.on('click', function () {
            $self.trigger('like');
        });

        self.render = function (currentFramework) {
            likeCountElem.text(currentFramework.likeCount);
            frameworkNameElem.text(currentFramework.name);
            logoImageElem.prop('src',currentFramework.imgSrc);
        }
    }

    function FrameworkListView() {

        var self = this;
        var $self = $(self);

        var frameworkListElem = $('#framework-list');

        self.render = function (frameworks) {
            var framework, elem, i;

            // Clear the list
            frameworkListElem.html('');

            for (i = 0; i < frameworks.length; i++) {
                framework = frameworks[i];

                elem = $('<li>');
                elem.text(framework.name);

                elem.on('click', (function (framework) {
                    // Capture context in a closure
                    return function () {
                        $self.trigger('item-selected', framework);
                    };
                })(framework));

                frameworkListElem.append(elem);
            }
        }
    }


    /* ======= Controller ======= */

    var controller = {

        init: function () {

            var model = createModel();

            // set initial framework
            model.setCurrentFramework(model.getFrameworkList()[0]);

            var frameworkListView = new FrameworkListView();
            var frameworkView = new FrameworkView();

            $(frameworkListView).on('item-selected', function(event, currentFramework){ setCurrentFramework(currentFramework)});
            $(frameworkView).on('like', incrementLikes);

            frameworkListView.render(model.getFrameworkList());
            frameworkView.render(model.getCurrentFramework());

            $(model).on('change', updateUi);

            function setCurrentFramework(framework) {
                model.setCurrentFramework(framework);
            }

            function incrementLikes() {
                model.likeCurrentFramework();
            }

            function updateUi() {
                // currently this re-renders the whole UI ... of course this should be optimized
                frameworkListView.render(model.getFrameworkList());
                frameworkView.render(model.getCurrentFramework());
            }
        }
    };

    // make it go!
    controller.init();
})();
