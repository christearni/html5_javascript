import {IRating} from "./Model";

export class RatingListView {
    private ratingList: JQuery;

    constructor() {
        this.ratingList = $('#ratings');

        this.ratingList.on('click', '.remove', e => {
            const removeId : number = $(e.target).parents('li').data('id'); // jQuery converts to number if possible
            $(this).trigger('ratingRemoved', removeId);
        });
    }

    public render(ratings: Array<IRating>) {
        this.ratingList.html('');
        for(var i = 0, len = ratings.length; i < len  ; i++ ){
            var rating = ratings[i];
            this.ratingList.append(
                '<li class="list-group-item" data-id="' + rating.id +'">' +
                '<div class="rating pull-xs-left">' +
                rating.grade +
                '</div>' +
                '<span>' +
                rating.name +
                '</span>' +
                '<span class="pull-xs-right">' +
                '<button class="btn btn-xs btn-danger remove fa fa-trash-o"></button>' +
                '</span>' +
                '</li>'
            );
        }

    }
}
