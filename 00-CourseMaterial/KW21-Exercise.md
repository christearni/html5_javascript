## Exercise 1: From a naive JavaScript Application to a modern Front-End Build:

### Part 1: Refactoring of the naive Application

- Change into the directory `01-Intro/01-SimplisticToDoNaive`
- Open `index.html` in a browser and add a new ToDo-Item
- Inspect the HTML and JavaScript code that make up the application
- Try to debug the JavaScript code
- Refactor the application: Separate the JavaScript and HTML code.


### Part 2: Modern Front-End Build

- Change into the directory `01-Intro/05-SimplisticToDoES6`
- Execute the steps of the frontend build as described in `README.md`
- Run the app in development mode, try to debug it
- Inspect the result of the build in the directory `public`. 
- Run the tests as scripts and optionally with IDE integration
- Implement the 'done' function (inspect the commented code) and the test for this functionality

