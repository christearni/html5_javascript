Install dependencies:

    yarn install

Run the example:

    yarn start

A browser should open, watch the dev tools console ...

Run a build:

    yarn run build

Observe:
- minification
- tree-shaking (elimination of unused functions)