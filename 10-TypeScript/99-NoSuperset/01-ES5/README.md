TypeScript is a superset of ES5.

Therefore you can just rename `app.js` to `app.ts` in WebStorm and the site still works.