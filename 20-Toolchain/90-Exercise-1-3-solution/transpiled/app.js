'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Greeter = function () {
    function Greeter() {
        _classCallCheck(this, Greeter);
    }

    _createClass(Greeter, [{
        key: 'getMessage',
        value: function getMessage() {
            return 'Hello from jQuery';
        }
    }]);

    return Greeter;
}();

var greeter = new Greeter();

$('body').append('<h1 class="alert alert-info"> ' + greeter.getMessage() + ' </h1>');
//# sourceMappingURL=app.js.map