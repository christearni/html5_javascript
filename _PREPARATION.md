# CAS Applikationsentwicklung mit JavaScript & HTML5 2017

### Important:


Make sure that you have unrestricted access to the internet! In the steps below you will have to access GitHub and npmjs.org. Some corporate proxies block these sites or block access over https/git!

If you are already using node/npm, make sure that you don't have a global npm-proxy configured. (Check: `npm config get proxy` should return `null`)

## Software Installation
For the workshop the following software should be installed.  

**The version numbers don't have to match exactly, but should be pretty close!**

### Git
A recent git installation is needed to download the sample code and exercises.  
Git can be downloaded from here: <https://git-scm.com/download/>

Check:  

	> git --version                                                             
	git version 2.12.2



### Node.js & NPM 
Node and NPM are the fundament for the JavaScript toolchain.  
The Node installer can be downloaded here: <http://nodejs.org/download/>

Install the latest version of Node.  
- Node version 7.10 or greater is preferred. 
- Node version > 6 is mandatory.

**Advanced instructions for OSX/Linux:** If you don't want to install global packages with `sudo` in the steps below, you can either install [Node Version Manager](https://github.com/creationix/nvm) (recommended) or you can perform the instructions here: <https://docs.npmjs.com/getting-started/fixing-npm-permissions>

Check:

	> node --version
	v7.10.0
	> npm --version
	4.2.0
	

### Global NPM Packages

We want to install some JavaScript development tools globally, so that they can be used from the commandline.

**OSX/Linux:**run the following commands:

	sudo npm install -g yarn lite-server http-server 
	sudo npm install -g eslint webpack-cli babel-cli typescript ts-node



**Windows:** run the following commands in a Administrator command prompt:

	npm install -g yarn lite-server http-server eslint webpack-cli
	npm install -g eslint webpack-cli babel-cli typescript ts-node


Ignore some scary warnings during the installation. Everything is ok, as long as the final output of the installation command is not an `npm ERR ...`
	
Background info: As default (if you did not execute the advanced instructions above to avoid `sudo`) the installation of those tools is placed in `/usr/local/lib/` on OSX/Linux or in `C:\Program Files\nodejs` on Windows. To uninstall the packages, you can always delete the directory `node_modules` there.




### Browser
A recent Chrome and/or Firefox Browser must be available.  



### WebStorm
The WebStorm IDE from JetBrains should be installed.  
There is a free 30 day trial version of WebStorm available here: <http://www.jetbrains.com/webstorm/>.  
Students can get a free licence at: https://www.jetbrains.com/student/

WebStorm is not a requirement for the workshop. However the examples and demos will be shown with WebStorm. It is up to the attendees to use any other editor of their preference.  
Note: IntelliJ IDEA Ultimate supports the same features as WebStorm.



## Course Material

All the course material will be provided in the following repository:

	https://bitbucket.org/jonasbandi/cas-html5-2017/
	
Please clone the repo like this:

	git clone https://bitbucket.org/jonasbandi/cas-html5-2017.git
	

To update the repo later:

	cd cas-html5-2017
	git pull
	



